package com.udrive.client.services;

import com.udrive.client.models.Car;
import com.udrive.client.models.CarBooking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CarService {
    @GET("cars/nearby/available")
    Call<List<Car>> fetch_nearby_available(@Query("latitude") double latitude, @Query("longitude") double longitude);

    @FormUrlEncoded
    @PUT("cars/{id}/book")
    Call<CarBooking> bookCar(@Path("id") String carId, @Field("email") String userEmail, @Field("booking_type") String bookingTpe, @Field("booking_hour") int bookingTimeHour, @Field("booking_minute") int bookingTimeMinute);
}
