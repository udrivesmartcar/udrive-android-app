package com.udrive.client.services;


import com.udrive.client.models.CarBooking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CarBookingService {
    @GET("car_bookings/user")
    Call<List<CarBooking>> fetchDrives(@Query("email") String email);
}
