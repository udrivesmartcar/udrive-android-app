package com.udrive.client;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;

import com.udrive.client.models.CarBooking;
import com.udrive.client.services.CarBookingService;
import com.udrive.client.view_adapters.CustomDrivesListAdapter;
import com.udrive.client.view_models.DriveInfo;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDrivesActivity extends AppCompatActivity {
    Geocoder geocoder;
    ArrayList<DriveInfo> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_drives);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        geocoder = new Geocoder(this, Locale.getDefault());

        CarBookingService carBookingService = RetrofitClient.getInstance().create(CarBookingService.class);
        Call<List<CarBooking>> carBookingServiceCall = carBookingService.fetchDrives(LoginActivity.loggedInUser.getEmail());
        carBookingServiceCall.enqueue(new Callback<List<CarBooking>>() {
            @Override
            public void onResponse(Call<List<CarBooking>> call, Response<List<CarBooking>> response) {
                List<CarBooking> carBookings = response.body();
                for (CarBooking carBooking : carBookings) {
                    DriveInfo driveInfo = new DriveInfo();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ", Locale.US);
                    format.setTimeZone(TimeZone.getTimeZone("IST"));
                    Date startDate = null;
                    try {
                        startDate = format.parse(carBooking.getStart_time());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    driveInfo.setStartTime(String.valueOf(startDate));
                    Address address = null;
                    try {
                        address = geocoder.getFromLocation(Double.parseDouble(carBooking.getStart_latitude()), Double.parseDouble(carBooking.getStart_longitude()), 1).get(0);
                        driveInfo.setStartLocation(address.getAddressLine(0));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        address = geocoder.getFromLocation(Double.parseDouble(carBooking.getEnd_latitude()), Double.parseDouble(carBooking.getEnd_longitude()), 1).get(0);
                        driveInfo.setEndLocation(address.getAddressLine(0));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    driveInfo.setStatus(carBooking.getStatus().toUpperCase());
                    list.add(driveInfo);

                    ListView listView = (ListView) findViewById(R.id.drives_list);
                    CustomDrivesListAdapter adapter = new CustomDrivesListAdapter(getApplicationContext(), 1, list);
                    listView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<CarBooking>> call, Throwable t) {
                System.out.println("halo");
            }
        });
    }

}
