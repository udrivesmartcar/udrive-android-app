package com.udrive.client.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CarBooking {
    private String id;
    private String start_time;
    private String start_latitude;
    private String end_latitude;
    private String start_longitude;
    private String end_longitude;
    private String status;
}
