package com.udrive.client.view_adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.udrive.client.R;
import com.udrive.client.view_models.DriveInfo;

import java.util.ArrayList;
import java.util.List;

public class CustomDrivesListAdapter extends ArrayAdapter {
    LayoutInflater lInflater;
    Context ctx;
    List<DriveInfo> list;

    public CustomDrivesListAdapter(Context context, int resource, ArrayList<DriveInfo> list) {
        super(context, resource, list);
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ctx = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder view;

        if(rowView == null)
        {
            rowView = lInflater.inflate(R.layout.activity_drives_list_view, null);

            view = new ViewHolder();
            view.startTime= (TextView) rowView.findViewById(R.id.start_time);
            view.startLocation= (TextView) rowView.findViewById(R.id.start_location);
            view.endLocation= (TextView) rowView.findViewById(R.id.end_location);
            view.status= (TextView) rowView.findViewById(R.id.status);

            rowView.setTag(view);
        } else {
            view = (ViewHolder) rowView.getTag();
        }

        /** Set data to Views */
        DriveInfo item = list.get(position);
        view.startTime.setText(item.getStartTime());
        view.startLocation.setText(item.getStartLocation());
        view.endLocation.setText(item.getEndLocation());
        String status = item.getStatus();
        view.status.setText(status);
        int color = Color.BLUE;
        if (status.toLowerCase().equals("scheduled")) {
            color = Color.BLUE;
        } else if (status.toLowerCase().equals("cancelled")) {
            color = Color.RED;
        } else if (status.toLowerCase().equals("closed")) {
            color = Color.GREEN;
        }
        view.status.setTextColor(color);
        return rowView;
    }

    protected static class ViewHolder{
        protected TextView startTime;
        protected TextView startLocation;
        protected TextView endLocation;
        protected TextView status;
    }
}