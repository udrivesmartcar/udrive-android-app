package com.udrive.client;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.udrive.client.models.Car;
import com.udrive.client.models.CarBooking;
import com.udrive.client.services.CarService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDriveActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener {
    // LogCat tag
    private static final String TAG = BookDriveActivity.class.getSimpleName();

    private static final LatLng INIT_POSITION = new LatLng(44.968046, -94.420307);

    // Directory name to store QR code
    public static final String QR_CODES = "QR codes";

    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private GoogleApiClient mGoogleApiClient;
    private Marker userLocationMarker;
    private LatLng userLocation;
    private String chosenCabId;
    private Marker chosenCabMarker;
    private Button bookingBtn;
    private HashMap<String, Marker> carMarkerMap = new HashMap<>();
    private String bookingType = "drive_now";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_drive);

        bookingBtn = (Button) findViewById(R.id.booking_button);
        bookingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performBooking();
            }
        });

        mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        Log.d(TAG, "*************Building Google API Client");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private Bitmap generateQRCode(String data) throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = writer.encode(data, BarcodeFormat.QR_CODE, 512, 512);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }

    private File getFileForQRCodeStorage() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                QR_CODES);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + QR_CODES + " directory");
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator
                    + "QR_CODE_" + timeStamp + ".png");
    }

    private void confirmBooking(String bookingType, int bookingTimeHour, int bookingTimeMinute) {
        bookingBtn.setVisibility(View.GONE);
        chosenCabMarker.setTitle("Your smart car awaits you");
        chosenCabMarker.showInfoWindow();
        try {
            Bitmap bmp = generateQRCode(chosenCabId);
            File file = getFileForQRCodeStorage();
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                Toast.makeText(getApplicationContext(),
                        "QR Code to unlock the car has been saved to your device", Toast.LENGTH_LONG)
                        .show();
                CarService carService = RetrofitClient.getInstance().create(CarService.class);
                Call<CarBooking> carServiceCall = carService.bookCar(chosenCabId, LoginActivity.loggedInUser.getEmail(), bookingType, bookingTimeHour, bookingTimeMinute);
                carServiceCall.enqueue(new Callback<CarBooking>() {
                    @Override
                    public void onResponse(Call<CarBooking> call, Response<CarBooking> response) {
                        CarBooking booking = response.body();
                        Log.d(TAG, "Response from server : Booking ID " + booking.getId());
                        Log.d(TAG, "Response from server : Booking Status " + booking.getStatus());
                    }

                    @Override
                    public void onFailure(Call<CarBooking> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private void initTimePicker(TimePicker timePicker) {
        try {
            timePicker.setIs24HourView(true);
            timePicker.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performBooking() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.booking_popup_menu);
        dialog.setTitle("Make your booking");

        final TimePicker bookTime = (TimePicker) dialog.findViewById(R.id.time_picker);
        initTimePicker(bookTime);

        final RadioGroup bookingTypeGroup = (RadioGroup) dialog.findViewById(R.id.booking_type);
        bookingTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton bookingType =(RadioButton)dialog.findViewById(checkedId);
                switch (bookingType.getId()) {
                    case R.id.book_now:
                        break;
                    case R.id.book_later:
                        bookTime.setEnabled(true);
                        break;
                }
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.confirm);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = bookingTypeGroup.getCheckedRadioButtonId();
                RadioButton bookingType = (RadioButton) dialog.findViewById(selectedId);
                dialog.dismiss();
                confirmBooking(bookingType.getText().toString(), bookTime.getCurrentHour(), bookTime.getCurrentMinute());
            }
        });

        Button dialogCancelButton = (Button) dialog.findViewById(R.id.cancel);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        if (map == null) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);

            mapFragment.getMapAsync(this);
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public void setUpMap(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            Log.d(TAG, "************Permission issue");
        }

        //fetch close by cab locations from server
        if (userLocation != null) {
            Log.d(TAG, "***********Fetching close by locations");
            CarService carService = RetrofitClient.getInstance().create(CarService.class);
            LatLng currentUserPosition = userLocationMarker.getPosition();
            Call<List<Car>> carServiceCall = carService.fetch_nearby_available(currentUserPosition.latitude, currentUserPosition.longitude);
            carServiceCall.enqueue(new Callback<List<Car>>() {
                @Override
                public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                    List<Car> cars = response.body();
                    Log.d(TAG, "Cars available : "+cars.size());
                    Collection<Marker> markers = carMarkerMap.values();
                    for (Car car : cars) {
                        boolean markerExists = false;
                        for(Marker marker : markers) {
                            if (String.valueOf(marker.getPosition().latitude).equals(car.getLatitude()) && String.valueOf(marker.getPosition().longitude).equals(car.getLongitude())) {
                                markerExists = true;
                                carMarkerMap.put(car.getId(), marker);
                                break;
                            }
                        }
                        if (!markerExists) {
                            LatLng coordinate = new LatLng(Double.parseDouble(car.getLatitude()), Double.parseDouble(car.getLongitude()));
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(coordinate)
                                    .title("")
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.book_drive_icon)));
                            carMarkerMap.put(car.getId(), marker);
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Car>> call, Throwable t) {

                }
            });
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Log.d(TAG, "************Firing marker clicked for "+ marker);
        if (carMarkerMap.values().contains(marker))
        {
            if (marker != null) {
                for (Map.Entry<String, Marker> entry : carMarkerMap.entrySet()) {
                    if (entry.getValue().getPosition().latitude == marker.getPosition().latitude &&
                            entry.getValue().getPosition().longitude == marker.getPosition().longitude) {
                        chosenCabId = entry.getKey().toString();
                        break;
                    }
                }
                LatLng pos = marker.getPosition();
                marker.remove();
                chosenCabMarker = map.addMarker(new MarkerOptions()
                        .position(pos)
                        .title("")
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("cab_booked",120,120))));
            }
        }

        return true;
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMarkerClickListener(this);
        Log.d(TAG, "************Init map");
        setUpMap();
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "************Firing onLocationChanged");
        double dLatitude = location.getLatitude();
        double dLongitude = location.getLongitude();
        // remove previous current location Marker
        if (userLocationMarker != null){
            userLocationMarker.remove();
        }
        userLocation = new LatLng(dLatitude, dLongitude);
        userLocationMarker = map.addMarker(new MarkerOptions()
                .position(userLocation)
                .title("You are here"));
        userLocationMarker.showInfoWindow();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 14));
        setUpMap();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "************onConnected");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            Log.d(TAG, "************Permission issue");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
