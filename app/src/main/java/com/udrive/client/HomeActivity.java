package com.udrive.client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
    // UI references.
    private TextView bookDriveView;
    private TextView viewDrivesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bookDriveView = (TextView) findViewById(R.id.book_drive);
        bookDriveView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookDrive();
            }
        });

        viewDrivesView = (TextView) findViewById(R.id.view_drives);
        viewDrivesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewDrives();
            }
        });
    }

    private void bookDrive() {
        Intent intent = new Intent(getApplicationContext(), BookDriveActivity.class);
        startActivity(intent);
    }

    private void viewDrives() {
        Intent intent = new Intent(getApplicationContext(), ViewDrivesActivity.class);
        startActivity(intent);
    }

}
